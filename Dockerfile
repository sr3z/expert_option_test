FROM python:3.8-buster as base
ENV PYTHONUNBUFFERED 1

FROM base as builder

RUN mkdir /install
WORKDIR /install

# Add python-libs.txt
COPY requirements.txt /tmp/

# Install dependencies
RUN apt-get update && apt-get install -y  \
        build-essential \
        python3-dev \
        libpq-dev \
        && \
        pip install --upgrade pip && pip install --prefix=/install -r /tmp/requirements.txt

ENV DOCKERIZE_VERSION v0.6.1

# Build from clean image
FROM base

# Install dependencies
RUN apt-get update && apt-get install bash

ENV LIBS /usr/lib
COPY --from=builder /install /usr/local

COPY . /code/

RUN mkdir -p /log
WORKDIR /code

CMD ["python", "base_app.py"]
