# relay_postgres_changes


# dB settings
DATABASES = {
    "rdv6088": {
        "host": "DB_IP",
        "name": "DBNAME",
        "user": "DB_USER",
        "password": "USER_PASS",
    },
}

SENTRY = {
    "DSN": "DSN_CODE"
}


# run test
python rdv_changes.py


cd /home/expert/pgsql
docker-compose down
docker-compose up -d


docker exec pgsql_postgresql-slave_1 touch /tmp/postgresql.trigger.5432 (edite
