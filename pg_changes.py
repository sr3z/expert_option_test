from datetime import timedelta
from lazy_logging import logger_factory
import settings
from base_app import PostgresqlRelayProcessor

TABLES_TO_MONITOR = ['User', 'Option', 'Transactions', 'StatLogin', 'real_option_archive', 'rdv6088_socialauth',
                     'z_gotobill', 'rdv6088_userbonusevent', 'rdv6088_user_bonus_balance',
                     'rdv6088_deal_used_bonus_amount', 'test_user_id', 'Asset', 'demo_option']
DATABASE = 'expertoption_core'
TOPIC_TEMPLATE = f"table_changes_%s_{DATABASE}"
WATCHDOG_INTERVAL = 600

LOGGER = logger_factory(
    'expertoption.relay_postgres_changes',
    is_private=True,
    level_stdout='DEBUG',
    log_root_path=settings.LOG_ROOT,
)


class PostgresqlRelayProcessor(PostgresqlRelayProcessor):
    DATABASE = DATABASE
    TABLES_TO_MONITOR = TABLES_TO_MONITOR
    TOPIC_TEMPLATE = TOPIC_TEMPLATE
    WATCHDOG_INTERVAL = WATCHDOG_INTERVAL
    LOGGER = LOGGER


if __name__ == "__main__":
    PostgresqlRelayProcessor().run()
