from __future__ import print_function
import sys
import json
import time
import logging
import traceback
from datetime import date, datetime
from functools import wraps
import psycopg2
import psycopg2.extras
from decimal import Decimal

from typing import Any
from typing import Optional
from typing import List
from lazy_logging import logger_factory

try:
    import settings
except Exception:
    traceback.print_exc()

# logger = logging.getLogger(__name__)
logger = logger_factory(
    "expertoption.relay_postgres_changes",
    is_private=True,
    level_stdout="DEBUG",
    log_root_path=settings.LOG_ROOT,
)
logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

try:
    from watchdog_client import WatchdogClient
except Exception as e:
    logger.error("IMPORT ERROR WATCHDOG: {}".format(e))

    class WatchdogClient():
        pass


def retry_on_exception(times: int = 0, exceptions: Optional[List[Exception]] = None):
    """Decorator which reruns function {times} times if exception from {exceptions} list occurred
    or any exception if not set
    @param times int of times to rerun function on exception occurred
    @param exceptions list of Exceptions to catch, will be treated as any exception on non set"""
    if exceptions is None:
        exceptions = [Exception]

    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for _ in range(times + 1):
                last_exception = None
                try:
                    return func(*args, **kwargs)
                except (*exceptions,) as exc:
                    last_exception = exc

            raise last_exception

        return wrapper

    return decorator


class ComplexEncoder(json.JSONEncoder):
    """Class extends JSONEncoder to dump Decimal, datetime objects and decodes bytes"""

    def default(self, o: Any) -> Any:
        if isinstance(o, Decimal):
            return str(o)
        if isinstance(o, datetime):
            return str(o)
        if isinstance(o, date):
            return str(o)
        if isinstance(o, bytes):
            return o.decode("utf-8")

        return json.JSONEncoder.default(self, o)


class PostgresqlRelayProcessor:
    DATABASE = {}
    DATABASE_NAME = ""
    POSTFIX = ""
    TOPIC_TEMPLATE = "table_changes_{}_{}{}"
    DB_TABLES = []

    def start_connection(self):
        #####################################
        # FIXME
        #####################################
        self.DATABASE["user"] = "postgres"
        self.DATABASE["password"] = "q123456"
        #####################################
        self.conn = psycopg2.connect("host={} port={} dbname={} user={} password={}".
                format(
                    self.DATABASE.get("host"),
                    self.DATABASE.get("port", 5432),
                    self.DATABASE_NAME,
                    self.DATABASE.get("user"),
                    self.DATABASE.get("password"),
                ),
                connection_factory=psycopg2.extras.LogicalReplicationConnection)
        self.cur = self.conn.cursor()

    def run(self) -> None:
        start_time = time.time()
        while True:
            try:
                self.start_connection()
                break
            except psycopg2.DatabaseError as e:
                spent = time.time() - start_time
                logger.error("CONSUME_ERROR: {}: {}".format(e, spent))
                if spent > 600:
                    return
                time.sleep(10)

        if not self.cur:
            logger.error("DB CURSOR IS NONE ..")
            return

        if self.DB_TABLES and self.DB_TABLES.get(self.DATABASE_NAME, []):
            table_name = self.DB_TABLES.get(self.DATABASE_NAME, ["test"])[0]
        else:
            table_name = "test"
        logger.info("Starting testing ..")
        SQL = "INSERT INTO {} (MSG) VALUES ('111') RETURNING ID".format(table_name)
        count = 0
        while True:
            try:
                results = []
                while True:
                    self.cur.execute(SQL)
                    id = self.cur.fetchone()[0]
                    logger.warning("{}".format(id))
                    results.append(str(id))
                    time.sleep(0.1)
                logger.warning("INSERTED: {}".format(results))

                start_time = time.time()
                spent = 0

            except psycopg2.DatabaseError as e:
                logger.error("{}: CONSUME_ERROR: {}".format(count, e))
                count += 1
                if count > 60:
                    break
                time.sleep(10)
                self.start_connection()
            except KeyboardInterrupt:
                self.cur.close()
                self.conn.close()
                break


if __name__ == "__main__":
    if len(sys.argv) < 2:
        logger.error("DATABASE_SET & DB REQUIRED")
        sys.exit(1)
    if not sys.argv[1]:
        logger.error("DATABASE_SET IS EMPTY")
        sys.exit(1)
    dbs = sys.argv[1].split(":")
    if not dbs:
        logger.error("DATABASE_SET IS INCORRECT: {}".format(dbs))
        sys.exit(1)
    if not hasattr(settings, dbs[0]):
        logger.error("DATABASE_SET IS MISSING: {}".format(dbs[0]))
        sys.exit(1)
    processor = PostgresqlRelayProcessor()
    dbset = getattr(settings, dbs[0])
    if len(dbs) > 1:
        processor.DATABASE_NAME = dbs[1]
    else:
        processor.DATABASE_NAME = "default"
    logger.warning(processor.DATABASE_NAME)

    if len(dbs) > 2:
        processor.POSTFIX = dbs[2]
    else:
        processor.POSTFIX = ""

    processor.DATABASE = dbset.get(processor.DATABASE_NAME)
    if hasattr(settings, "DB_TABLES"):
        processor.DB_TABLES = getattr(settings, "DB_TABLES")
    else:
        processor.DB_TABLES = []
    processor.run()
