REDIS_CONNECTIONS = {
    "default": "",
}
REDIS_CHANNELS_MAP = {}
WATCHDOG_URL = "http://localhost:8090/"
KAFKA = {
    "BOOTSTRAP_SERVERS": "localhost:9092",
}
