import json
from json.decoder import JSONDecodeError

from watchdog_client import WatchdogClient
from lazy_logging import logger_factory
from kafka import KafkaConsumer
from kafka import TopicPartition
from kafka.consumer.fetcher import ConsumerRecord

import settings


from pprint import pprint
pprint(settings.WATCHDOG_URL)

CONSUMER_TIMEOUT_MS = 2 * 1000
KAFKA_GROUP_ID = "mysqchanges_processor_1"

TABLES_TO_MONITOR = ['User',]
TOPIC_TEMPLATE = "db_table_changes_%s"
WATCHDOG_INTERVAL = 600

LOGGER = logger_factory(
    'expertoption.mysql_changes_processor',
    is_private=True,
    level_stdout='DEBUG',
    log_root_path=settings.LOG_ROOT,
)


class PostgresqlChangesProcessor:
    def __init__(self):
        self._consumer = None
        self._watchdog = WatchdogClient(
            service_name=self.__class__.__name__,
            url=settings.WATCHDOG_URL,
            logger=LOGGER
        )


    def deserialize_value(self, value: bytes) -> dict:
        """Deserializes body of kafka record
        @param value input data of message received in topic
        @return dict of deserialized message"""
        return json.loads(value.decode("utf-8"))

    def create_kafka_consumer(self) -> None:
        """Creates connection to Kafka"""
        self._consumer = KafkaConsumer(
            enable_auto_commit=False,
            bootstrap_servers=settings.KAFKA["BOOTSTRAP_SERVERS"],
            group_id=KAFKA_GROUP_ID,
            client_id='PostgresqlChangesProcessor',
            api_version=(0, 9),
            consumer_timeout_ms=CONSUMER_TIMEOUT_MS,
            auto_offset_reset='latest'
        )

    @property
    def get_topics_list(self) -> dict:
        """Generates list of topics based on TABLES_TO_MONITOR
        @return list of str topics to subscribe to"""
        return [TOPIC_TEMPLATE % (x, ) for x in TABLES_TO_MONITOR]

    def kafka_subscribe(self):
        """Subscribes to topics one topic per monitoring table"""
        topics = [TopicPartition(x, 0) for x in self.get_topics_list]
        # assign instead of subscribe to prevent use of kafka groups and to have standalone consumers
        self._consumer.assign(topics)

    def process_message(self, message: ConsumerRecord, value: dict) -> None:
        """Here message have to be processed
        @param message ConsumerRecord of record with raw value and other info
        @param value dict of deserialized message.value"""
        print(message)

    def update_offset(self) -> None:
        """Runs if message processed successfully to update offset"""
        self._consumer.commit()

    def run(self):
        self.create_kafka_consumer()
        self.kafka_subscribe()

        while True:
            for message in self._consumer:
                try:
                    value = self.deserialize_value(message.value)
                    self.process_message(message, value)
                except JSONDecodeError:
                    LOGGER.exception('', extra=dict(data=message))
                    return
                except Exception:
                    LOGGER.exception('')
                    return
                self.update_offset()
            self._watchdog.ok(WATCHDOG_INTERVAL)


if __name__ == "__main__":
    PostgresqlChangesProcessor().run()
